<?php

namespace Brightly\Mango;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;

class NovaServiceProvider extends ServiceProvider
{
    /** Bootstrap any application services.
     * 
     * @param \Illuminate\Contracts\Http\Kernel  $kernel
     *
     * @return void
     */
    public function boot(Kernel $kernel)
    {
        $this->registerPublishing();

        // $this->registerResources();

        //Blade::component('app.components.component', 'component');
    }

    public function registerPublishing()
    {
        $this->publishes([
            __DIR__.'/../nova' => base_path('nova'),
        ], 'mango-nova-dev');

        $this->publishes([
            __DIR__.'/Nova' => app_path('Nova'),
        ], 'mango-nova-resource');

        $this->publishes([
            __DIR__.'/Providers' => app_path('Providers'),
        ], 'mango-nova-providers');

        $this->publishes([
            __DIR__.'/../database/migrations' => database_path('migrations')
        ], 'mango-nova-migrations');

        $this->publishes([
            __DIR__.'/../resources/views'   => resource_path('views'),
            __DIR__.'/../resources/vendor'  => resource_path('vendor')
        ], 'mango-nova-views');

        $this->publishes([
            __DIR__.'/../config/nova.php' => config_path('nova.php'),
        ], 'mango-nova-config');

    }
}