<p class="mt-8 text-center text-xs text-80">
    <a href="https://brightly.hu" class="text-primary dim no-underline">Brightly</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Brightly.
    <span class="px-1">&middot;</span>
    Laravel Nova v{{ \Laravel\Nova\Nova::version() }}, Brightly Mango v{{ \Brightly\Mango\MangoServiceProvider::version() }}
</p>
