<router-link tag="h3" :to="{name: 'mango-nova-menu'}" class="cursor-pointer flex items-center font-normal dim text-white mb-6 text-base no-underline">
    <svg xmlns='http://www.w3.org/2000/svg' class="sidebar-icon" width='20' height='20' viewBox='0 0 512 512'><line x1='80' y1='160' x2='432' y2='160' style='fill:none;stroke:var(--sidebar-icon);stroke-linecap:round;stroke-miterlimit:10;stroke-width:32px'/><line x1='80' y1='256' x2='432' y2='256' style='fill:none;stroke:var(--sidebar-icon);stroke-linecap:round;stroke-miterlimit:10;stroke-width:32px'/><line x1='80' y1='352' x2='432' y2='352' style='fill:none;stroke:var(--sidebar-icon);stroke-linecap:round;stroke-miterlimit:10;stroke-width:32px'/></svg>
    <span class="sidebar-label">
        {{ __('Punci') }}
    </span>
</router-link>