<p align="center"><img src="https://repositories.brightlydev.hu/images/mango.jpg" width="200"></p>

# Mango Nova v1.0.0 ("Next Summer" edition)

## Intruduction
Mango Nova package provides Nova's functionality for a given project. Basic functionality, like Menus and Content are provided by the Nova package, this package is just a bridge to bind Nova to Mango based project.

### Install
First we have to install Mango Nova:
```bash
composer require brightly/mango-nova:"dev-feature/next-summer"
```
Mango Nova provides a lot of stuff, by default, like a Laravel Nova package. So, after yous installed Mango Nova, you have to publish some different part of that:
```bash
artisan vendor:publish --tag=mango-nova-dev
artisan vendor:publish --tag=mango-nova-views
artisan vendor:publish --tag=mango-nova-migrations
```
...and then you just should add to your `composer.json` the next lines:
```json
require-dev: {
    ...
    "laravel/nova": "*"
},
repositories:
[
    ...
    {
        "type": "path",
        "url": "./nova"
    }
]
```
...and of course, run:
```bash
composer update 
```
This is a bigger update: This will install the full [Laravel Nova](https://nova.laravel.com) package. So, now we have to set up Nova with the pre-built Mango classes to get everything work well together.
To have everything installed well, first you'll have to install Nova after `composer update`:
```bash
php artisan nova:install
```
Next step:
```bash
composer require anaseqal/nova-sidebar-icons:"^0.0.3"
```
...and then...
```bash
php artisan vendor:publish --provider="Anaseqal\\NovaSidebarIcons\\ToolServiceProvider" --force
```
Register the tool in `NovaServiceProvider`:
```php
use Anaseqal\NovaSidebarIcons\NovaSidebarIcons;
...

public function tools()
{
    return [
        new NovaSidebarIcons,
        ...
    ];
}
```

We also need some composer packages:
```bash
composer require benjaminhirsch/nova-slug-field:"^1.2"
composer require michielkempen/nova-order-field:"^2.0"
composer require ebess/advanced-nova-media-library:"2.9.1"
```
...and at least, some other settings already:
```bash
php artisan vendor:publish --tag=nova-media-library
php artisan vendor:publish --provider="Spatie\MediaLibrary\MediaLibraryServiceProvider" --tag="migrations"
php artisan vendor:publish --tag=mango-nova-providers
php artisan vendor:publish --tag=mango-nova-config
```

#### Migrations
Migrations added by default right after installed by the Composer.
>>>
:warning:

**Migrations are very important!**: The Mango uses UUID as primary key for _all_ models under `\Brightly\Mango\Models` namespace. This means, this is not compatible with the integer type IDs, so we also have to change Nova's field.
>>>

```bash
artisan migrate
```

### Usage
return [
            // new \Brightly\MangoNovaMenu\MangoNovaMenu(),
            new NovaSidebarIcons,
        ];2013
